package repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import model.Accomodation;
import model.Period;

public class BookingRepository {
	
	private List<Accomodation> accomodations;
	
	
	public BookingRepository(List<Accomodation> bookings) {
		this.accomodations = bookings;
	}


	public List<Accomodation> getAllAccomodations() {
		return this.accomodations;
	}
	
	
	public List<Accomodation> getAvailableAccomodationsForPeriod(Period period) {
		return accomodations.stream()
				.filter(accomodation -> accomodation.isAvailable(period))
				.collect(Collectors.toList());
	}
	
	
	public List<Accomodation> getAccomodationsForPrice(BigDecimal price) {
		return accomodations.stream()
				.filter(accomodation -> accomodation.getPriceForDay().compareTo(price) <= 0)
				.collect(Collectors.toList());
	}
	
	
	public void updateAccomodation(Accomodation accomodationToUpdate, Period period) {
		
		for (Accomodation accomodation : accomodations) {
	        if (accomodation.id() == accomodationToUpdate.id()) {
	        	accomodation.addReservedPeriod(period);
	        }
	    }
	}
	
}
