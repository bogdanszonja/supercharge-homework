package repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import model.Accomodation;
import model.Booking;
import model.Period;

public class DummyBookingRepository {
	
	public static List<Accomodation> createDummyData() {
		List<Accomodation> accomodations = new ArrayList<>();
		
		List<Period> reservedPeriodForFirstBooking = new ArrayList<>();
		
		reservedPeriodForFirstBooking.add(new Period(LocalDate.of(2020, 10, 10), LocalDate.of(2020, 10, 15)));
		reservedPeriodForFirstBooking.add(new Period(LocalDate.of(2020, 12, 24), LocalDate.of(2020, 12, 30)));
		
		List<Period> reservedPeriodForSecondBooking = new ArrayList<>();
		
		reservedPeriodForSecondBooking.add(new Period(LocalDate.of(2020, 9, 1), LocalDate.of(2020, 9, 12)));
		reservedPeriodForSecondBooking.add(new Period(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 11)));
		
		accomodations.add(new Accomodation(reservedPeriodForFirstBooking, BigDecimal.valueOf(657.95)));
		accomodations.add(new Accomodation(reservedPeriodForSecondBooking, BigDecimal.valueOf(6123.05)));
		
		return accomodations;
	}

}
