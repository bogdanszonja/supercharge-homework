package application;
import java.time.LocalDate;
import java.util.List;

import model.Accomodation;
import model.Booking;
import model.Period;
import model.User;
import print.BookingHistoryPrinter;
import repository.BookingRepository;
import repository.DummyBookingRepository;

public class BookingApplication {
	
	public static void main(String[] args) { 
		User user = new User();
		
		List<Accomodation> dummyData = DummyBookingRepository.createDummyData();
		
		BookingRepository bookingRepository = new BookingRepository(dummyData);
		
		Period period = new Period(LocalDate.of(2020, 10, 20), LocalDate.of(2020, 10, 25));
		
		List<Accomodation> availableAccomodations = bookingRepository.getAvailableAccomodationsForPeriod(period);
		
		user.makeBooking(period, availableAccomodations.get(0));
		
		BookingHistoryPrinter.printHistoryForUser(user);
    } 

}
