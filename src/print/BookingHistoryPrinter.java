package print;

import model.User;

public class BookingHistoryPrinter {
	
	public static void printHistoryForUser(User user) 
	{
		user.getBookingHistory().getBookings()
				.forEach(booking -> System.out.println(booking.toString()));
	}

}
