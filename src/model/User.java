package model;

public class User {

	private BookingHistory bookingHistory = new BookingHistory();
	

	public BookingHistory getBookingHistory() {
		return this.bookingHistory;
	}
	
	
	public void makeBooking(Period period, Accomodation accomodation) {
		Booking newBooking = new Booking(period, accomodation.getPriceForDay());
		this.bookingHistory.makeBooking(newBooking);
	}
	
	
	public void cancelBooking(Booking booking) {
		this.bookingHistory.cancelBooking(booking);
	}
}
