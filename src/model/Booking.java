package model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List; 

public class Booking {
	
	private Period reservedPeriod;
	
	private BigDecimal priceForDay;
	
	
	public Booking(Period reservedPeriod, BigDecimal priceForDay) {
		this.reservedPeriod = reservedPeriod;
		this.priceForDay = priceForDay;
	}


	public BigDecimal getPriceForDay() {
		return priceForDay;
	}

	
	@Override
	public String toString() {
	    StringBuilder stringBuilder = new StringBuilder();
	    
	    stringBuilder.append("Reserved period: " + this.reservedPeriod);
	    stringBuilder.append(" ");
	    stringBuilder.append("Price per day: " + this.priceForDay);
	
	    return stringBuilder.toString();
	}
	
}
