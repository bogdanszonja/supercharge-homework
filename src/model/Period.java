package model;

import java.time.LocalDate;

public class Period {

	private LocalDate fromDate;
	
	private LocalDate toDate;


	public Period(LocalDate fromDate, LocalDate toDate) {
		this.fromDate = fromDate;
		this.toDate = toDate;
	}

	
	public LocalDate getFromDate() {
		return fromDate;
	}


	public LocalDate getToDate() {
		return toDate;
	}
	
	
	@Override
	public String toString() {
	    StringBuilder stringBuilder = new StringBuilder();
	    
	    stringBuilder.append("from: " + this.fromDate);
	    stringBuilder.append(" ");
	    stringBuilder.append("to: " + this.toDate);
	
	    return stringBuilder.toString();
	}
	

	@Override
	public boolean equals(Object other) {
		if (other instanceof Period) {			
			if (((Period) other).getFromDate().equals(this.fromDate) && ((Period) other).getToDate().equals(this.toDate)) {
				return true;
			}
		}
		
		return false;
	}
	
}
