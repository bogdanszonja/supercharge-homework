package model;

import java.util.ArrayList;
import java.util.List;

public class BookingHistory {

	private List<Booking> bookings = new ArrayList<>();

	
	public List<Booking> getBookings() {
		return this.bookings;
	}
	
	
	public void makeBooking(Booking booking) {
		this.bookings.add(booking);
	}


	public void cancelBooking(Booking booking) {
		this.bookings.remove(booking);
	}
}
