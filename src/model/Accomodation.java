package model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Accomodation {
	
	private int id;
	
	private List<Period> reservedPeriods = new ArrayList<>();
	
	private BigDecimal priceForDay;
	
	
	public Accomodation(List<Period> reservedPeriods, BigDecimal priceForDay) {
		this.reservedPeriods = reservedPeriods;
		this.priceForDay = priceForDay;
	}

	
	public int id() {
		return this.id;
	}
	

	public BigDecimal getPriceForDay() {
		return priceForDay;
	}
	
	
	public void addReservedPeriod(Period period) {
		this.reservedPeriods.add(period);
	}
	

	public boolean isAvailable(Period period) {
		return !this.reservedPeriods.contains(period);
	}

}
